#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Kaalleen]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Snap Nodes - Snap selected nodes to the position of the first selected node
# Appears Under Extensions > Modify Path > Snap Selected Nodes
# An Inkscape 1.2+ extension
##############################################################################


from inkex import EffectExtension, errormsg


class SnapNodes(EffectExtension):

    def effect(self):

        selection_list = self.svg.selected
        selected_nodes = self.options.selected_nodes

        if len(selected_nodes) < 2:
            errormsg('Please select at least two nodes.')
            return

        node_pos = None
        for element in selection_list:
            transform = element.composed_transform()
            path = element.path.to_absolute().transform(transform)
            csp = path.to_superpath()

            for item in selected_nodes:
                path_id, subpath_index, node_index = item.split(':')
                if element.get('id') == path_id:
                    node = csp[int(subpath_index)][int(node_index)]
                    if node_pos is None:
                        node_pos = node[1]
                    csp[int(subpath_index)][int(node_index)][1] = node_pos

            element.set('d', csp.to_path().transform(-transform))


if __name__ == '__main__':
    SnapNodes().run()