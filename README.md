# Snap Nodes

Select at least two nodes and run `Extensions > Modify Path > Snap Selected Nodes`

This will snap all selected nodes to the position of the first selected node.

Works with Inkscape 1.2+
